# [EasyURLBot](https://t.me/easyurlbot)
[t.me/easyurl](https://t.me/easyurlbot)
> 縮網址服務 Telegram Bot

## 使用方法
傳入一段網址，Bot會回傳縮短後的網址。

## 目前支援的縮網址服務
-  [TinyURL](https://tinyurl.com)

## 可用指令
- **/help**
顯示 Bot 使用說明

## 部屬服務
### 環境
- Google Apps Script
### 部屬注意事項
- 需自行定義常數`botToken`為 Bot API Token
  ```javascript
  // Bot API Token
  const botToken = "xxxxxxxxx:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
  ```
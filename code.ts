const botURL = "https://api.telegram.org/bot";

const URLBannedError = {
    name: "URLBannedError",
    message: "此網域已被封鎖"
};

const RequestError = {
    name: "RequestError",
    message: "要求錯誤"
};

const UnknowError = {
    name: "UnknowError",
    message: "發生未知的錯誤"
};

const ConvertFailError = {
    name: "ConvertFailError",
    message: "轉換失敗"
};

function shortByTinyURL(url: string): string {
    const tinyurlHost = "https://tinyurl.com/create.php";
    const requestURL = `${tinyurlHost}?url=${url}`;
    const response = UrlFetchApp.fetch(requestURL, { muteHttpExceptions: true });
    const body = response.getContentText();
    const respCode = response.getResponseCode();
    let result = "";

    switch (respCode) {
        case 200:
            let match = /https:\/\/tinyurl.com\/(\w{4,})"/g.exec(body);
            if (match != null) {
                result = match[0];
                result = result.substr(0, result.length - 1);
            } else {
                throw ConvertFailError;
            }
            break;
        case 400:
            let isBanned = /URL domain banned/g.test(body);

            if (isBanned) {
                throw URLBannedError;
            } else {
                throw RequestError;
            }
        default:
            throw UnknowError;
    }

    return result;
}

function sendText(id: string, text: string) {
    const options: GoogleAppsScript.URL_Fetch.URLFetchRequestOptions = {
        method: "post",
        payload: { "text": text }
    };

    const response = UrlFetchApp.fetch(`${botURL}${botToken}/sendMessage?chat_id=${id}`, options);
}

function messageHandle(message) {
    const chatId = message["chat"]["id"];
    const text = message["text"];
    const msgEntities = message["entities"];

    if (msgEntities != null) {
        if (msgEntities[0]["type"] == "bot_command") {
            if (text == "/help" || text == "/start") {
                sendText(chatId,
                    `縮網址 Easy URL

使用方法：貼上一段網址，Bot會回傳縮短後的網址。

目前支援的縮網址類型：
    - tinyurl.com

————— Inline 模式 —————
使用方法：@easyurlbot <要縮短的網址>
範例：@easyurlbot www.googl.com

註：因 Inline Mode API 之限制，故網址字數上限為255個字。
—————————————————
                    
                    
Developer: @WeiKeUp`
                );
            }
        } else if (msgEntities[0]["type"] == "url") {
            try {
                const result = shortByTinyURL(text);
                sendText(chatId, result);
            } catch (ex) {
                if (ex == URLBannedError) {
                    sendText(chatId, `${ex.message}`);
                } else {
                    sendText(chatId, `${ex.message}，請再試一次`);
                }
            }
        }
    }
}

function inlineHandle(inlineQuery) {
    const queryId = inlineQuery["id"];
    const text = inlineQuery["query"];

    try {
        let result = shortByTinyURL(text);

        const answer = {
            cache_time: 0,
            switch_pm_text: "",
            switch_pm_parameter: "",
            results: {}
        };
        if (text.length >= 255) {
            result = "字數超過上限(255字)";
            answer.switch_pm_text = "點此移至機器人聊天室";
            answer.switch_pm_parameter = "a";
        } else {
            try {
                result = shortByTinyURL(text);
            } catch (ex) {
                if (ex == URLBannedError) {
                    result = ex.message;
                } else {
                    result = `${ex.message}，請再試一次`;
                }
            }
        }
        answer.results = JSON.stringify([
            {
                "type": "article",
                "id": 1,
                "title": result,
                "input_message_content": {
                    "message_text": result
                }
            }
        ]);

        const options: GoogleAppsScript.URL_Fetch.URLFetchRequestOptions = {
            method: "post",
            payload: answer,
        };

        const response = UrlFetchApp.fetch(`${botURL}${botToken}/answerInlineQuery?inline_query_id=${queryId}`, options);
    } catch (ex) { }
}

function doPost(e) {
    const data = JSON.parse(e.postData.contents);

    if (data["message"] != null) {
        messageHandle(data["message"]);
    } else if (data["inline_query"] != null) {
        inlineHandle(data["inline_query"]);
    }
}